import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { MCollapsibleDirective } from './shared/materialize/collapsible/directives/m-collapsible.directive';
import { ModalService } from './shared/materialize/modal';
import { AppStore } from './store';
import { logout } from './shared/security/session.slice';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  logout() {
    AppStore.dispatch(logout())
  }
  @ViewChild(MCollapsibleDirective, { static: true }) mCollapsible: MCollapsibleDirective | undefined
  title = 'productivity';

  @ViewChild("vcr", { read: ViewContainerRef }) vcr: ViewContainerRef | undefined

  constructor(
  ) { }

  ngAfterViewInit() {
    console.log(this.mCollapsible)
    // this.mCollapsible?.Options.accordion

    this.mCollapsible?.open(2)

    // if (this.vcr) {
    //   this.$modal.open(this.vcr, ContinueDialogComponent, (data: any) => console.log(data))
    // }

  }
}
