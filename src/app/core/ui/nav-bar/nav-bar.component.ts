import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable, map } from 'rxjs';
import { SessionService } from 'src/app/shared/security/services/session.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavBarComponent {

  get IsAuth$(): Observable<boolean> {
    return this.$session.Token$.pipe(map((token) => !!token))
  }

  constructor(
    private $session: SessionService
  ) { }
}
