import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-continue-dialog',
  templateUrl: './continue-dialog.component.html',
  styleUrls: ['./continue-dialog.component.scss']
})
export class ContinueDialogComponent {
  @Output() closeEvent = new EventEmitter<any>();

}
