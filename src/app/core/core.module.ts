import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './ui/nav-bar/nav-bar.component';
import { RouterModule } from '@angular/router';
import { ContinueDialogComponent } from './ui/continue-dialog/continue-dialog.component';



@NgModule({
  declarations: [
    NavBarComponent,
    ContinueDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    NavBarComponent,
    ContinueDialogComponent
  ]
})
export class CoreModule { }
