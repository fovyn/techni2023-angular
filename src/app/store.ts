import { configureStore } from "@reduxjs/toolkit";
import sessionReducer from './shared/security/session.slice'

export const AppStore = configureStore({
    reducer: {
        session: sessionReducer
    }
})