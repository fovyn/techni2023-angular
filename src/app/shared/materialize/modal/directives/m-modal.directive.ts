import { Directive, ElementRef } from '@angular/core';
import * as M from 'materialize-css'

@Directive({
  selector: '[mModal]'
})
export class MModalDirective {
  private instance: M.Modal | undefined

  constructor(
    private $elRef: ElementRef<any>
  ) {
    this.instance = M.Modal.init($elRef.nativeElement)

    this.instance.open()
  }

}
