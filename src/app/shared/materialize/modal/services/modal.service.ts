import { Injectable, Type, ViewContainerRef } from '@angular/core';

@Injectable()
export class ModalService {

  constructor(
  ) { }

  open($vcr: ViewContainerRef, type: Type<any>, callback?: Function) {
    const component = $vcr.createComponent(type)

    component.instance.closeEvent?.subscribe(callback)
  }
}
