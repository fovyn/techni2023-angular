import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalService } from './services/modal.service';
import { ModalContainerComponent } from './components/modal-container/modal-container.component';



@NgModule({
  declarations: [
    ModalContainerComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [ModalService]
})
export class ModalModule { }
