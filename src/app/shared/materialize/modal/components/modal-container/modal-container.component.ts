import { ChangeDetectionStrategy, Component, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  template: '<ng-template #modalContainer></ng-template>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalContainerComponent {
  @ViewChild("modalContaienr", { read: ViewContainerRef })
  protected modalContainer: ViewContainerRef | undefined

}
