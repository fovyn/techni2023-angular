import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MCollapsibleDirective } from './directives/m-collapsible.directive';



@NgModule({
  declarations: [MCollapsibleDirective],
  imports: [
    CommonModule
  ],
  exports: [MCollapsibleDirective]
})
export class CollapsibleModule { }
