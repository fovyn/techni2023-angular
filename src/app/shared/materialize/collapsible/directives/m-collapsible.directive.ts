import { Directive, ElementRef, Input } from '@angular/core';
import * as M from 'materialize-css'

@Directive({
  selector: '[mCollapsible]'
})
export class MCollapsibleDirective {
  private instance: M.Collapsible | undefined
  private options: Partial<M.CollapsibleOptions> = {}

  @Input('options') set Options(v: Partial<M.CollapsibleOptions>) {
    this.options = { ...this.options, ...v }
  }

  get Element(): HTMLUListElement { return this.$elRef.nativeElement }

  constructor(
    private $elRef: ElementRef<HTMLUListElement>
  ) { }

  ngOnInit() {
    this.instance = M.Collapsible.init(this.Element, this.options)
  }

  open(n: number) {
    this.instance?.open(n)
  }
}
