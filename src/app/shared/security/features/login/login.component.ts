import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AppStore } from 'src/app/store';
import { login } from '../../session.slice';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthService]
})
export class LoginComponent {

  constructor(
    private $auth: AuthService
  ) {
    AppStore.dispatch(login({ id: 1, token: '42' }))
    // this.$auth.register('flavian.ovyn@bstorm.be', 'blop@1234=')
    // this.$auth
    //   .login('flavian.ovyn@bstorm.be', 'blop@1234=')
    //   .subscribe(user => console.log(user))
  }
}
