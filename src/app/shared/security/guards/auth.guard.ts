import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateFn, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';
import { AppStore } from 'src/app/store';

// @Injectable({
//   providedIn: 'root'
// })
// export class AuthGuard {

//   constructor(
//     private $session: SessionService,
//     private $router: Router
//   ) { }

//   canActivate(
//     route: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

//     const token = this.$session.Token;

//     if (!token) {
//       return this.$router.navigateByUrl('/login')
//     }

//     return true;
//   }

// }

export function authGuard(): CanActivateFn {
  return () => {
    const { session } = AppStore.getState();

    console.log(session)

    return !!(session?.token)
  }
}
