import { Inject, inject } from "@angular/core"
import { PayloadAction, Reducer, createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { AuthService } from "./services/auth.service"
import { Router } from "@angular/router"

interface SessionState {
    id: number | null,
    token: string | null
}

const initialState: SessionState = { id: null, token: null }

const SessionSlice = createSlice({
    name: 'session-slice',
    initialState,
    reducers: {
        login: (state, { payload }: PayloadAction<{ id: number, token: string }>) => {
            if (state) {
                state.id = payload.id
                state.token = payload.token
            } else {
                state = payload
            }
            localStorage.setItem("session", JSON.stringify(state))
        },
        logout: (state) => {
            localStorage.removeItem("session")
            state = { id: null, token: null }
        }
    }
})

export const { login, logout } = SessionSlice.actions
export default SessionSlice.reducer as Reducer<SessionState | null>